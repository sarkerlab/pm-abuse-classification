# NONMEDICAL PRESCRIPTION MEDICATION USE CLASSIFICATION (TWITTER) #

This repository provides access to the archived code/data/models associated with the following publication:

Al-Garadi MA, Yang YC, Cai H, Ruan Y, O'Connor K, Graciela GH, Perrone J, Sarker A. Text classification models for the automatic detection of nonmedical prescription medication use from social media. BMC Med Inform Decis Mak. 2021 Jan 26;21(1):27. doi: 10.1186/s12911-021-01394-0. PMID: 33499852; PMCID: PMC7835447.

To access the paper, [click here](https://pubmed.ncbi.nlm.nih.gov/33499852/)

### Classifiers ###

The paper compares multiple classifiers:

* Support vector machines (SVM), random forest, Gaussian naive Bayes, neural networks (shallow), convolutional neural networks (deep), character level convolutional neural networks (deep), BiLSTM with Twitter GloVe embeddings, BERT, RoBERTa, AlBERT, XLNet, DistilBERT.
* The best performing classifier was a fusion based ensemble, also described in the paper. 

### All models and files for reproducibility###

The entire set of codes and models can be accessed from [here](https://drive.google.com/file/d/12SSWJ7Ttrq7eomZf2x0jufd_vr1Ztrfn/view)

Details of the dataset is available from a prior publication that can be accessed [here](https://pubmed.ncbi.nlm.nih.gov/32130117/)

### Data usage policy ###
All annotated tweets have been *reposted* via our lab's Twitter account:[@SarkerLab](https://twitter.com/SarkerLab). Please follow the following procedures for using the data:

* The above account is intentionally protected. This mechanism of sharing was agreed upon by our funding body (National Institute on Drug Abuse).
* All annotated texts should be retrieved only from this account.
* Please email Dr. Al-Garadi (m.a.al-garadi@emory.edu) or Dr. Sarker (abeed@dbmi.emory.edu) to obtain a data use agreement form. Emailed consent is also acceptable.
* Once data use is permitted, send a request to follow the abovementioned Twitter account. Once the invite is accepted, data from this profile can be downloaded using standard Twitter API functions.
* Do not re-share the data. 

### Funding ###
National Institute on Drug Abuse (NIDA).
